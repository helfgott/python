import random

def game():
    player = input("rock, paper or scissors? ")
    pc = ["rock", "paper", "scissors"]
    pc_choice = random.choice(pc)
    print(f"you chose {player}")
    print(f"the pc chose {pc_choice}")
    print("")
    if player == "rock" and pc_choice == "paper":
        print("you lose")
    elif player == "rock" and pc_choice == "scissors":
        print("you win!")
    elif player == "rock" and pc_choice == "rock":
        print("draw")
    elif player == "paper" and pc_choice == "paper":
        print("draw")
    elif player == "scissors" and pc_choice == "scissors":
        print("draw")
    elif player == "paper" and pc_choice == "rock":
        print("you win!")
    elif player == "paper" and pc_choice == "scissors":
        print("you lose")
    elif player == "scissors" and pc_choice == "paper":
        print("you win!")
    elif player == "scissors" and pc_choice == "rock":
        print("you lose")
    print("")
    new_game()

def new_game():
    ask_new_game = input("another game? ")
    if ask_new_game == "yes":
        game()
    else:
        exit()

game()